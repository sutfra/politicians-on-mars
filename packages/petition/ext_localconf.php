<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
        function () {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
                    'PoliticiansOnMars.petition',
                    'SignatureList',
                    [
                            'Signature' => 'list'
                    ],
                    // non-cacheable actions
                    [
                            'Signature' => 'list'
                    ]
            );
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
                    'PoliticiansOnMars.petition',
                    'SignatureLatest',
                    [
                            'Signature' => 'latest'
                    ],
                    // non-cacheable actions
                    [
                            'Signature' => 'latest'
                    ]
            );

            // wizards
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
                    'mod {
                        wizards.newContentElement.wizardItems.plugins {
                            elements {
                                signatureList {
                                    iconIdentifier = petition-plugin-signature
                                    title = LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.list.name
                                    description = LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.list.description
                                    tt_content_defValues {
                                        CType = list
                                        list_type = petition_signaturelist
                                    }
                                }
                                signatureLatest {
                                    iconIdentifier = petition-plugin-signature
                                    title = LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.latest.name
                                    description = LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.latest.description
                                    tt_content_defValues {
                                        CType = list
                                        list_type = petition_signaturelatest
                                    }
                                }
                            }
                            show = *
                        }
                   }'
            );
            $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

            $iconRegistry->registerIcon(
                    'petition-plugin-signature',
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:petition/Resources/Public/Icons/Plugin.svg']
            );
            $iconRegistry->registerIcon(
                    'petition-table-signature',
                    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                    ['source' => 'EXT:petition/Resources/Public/Icons/Extension.svg']
            );

        }
);
