# Petition

This very simple extension is designed to collect signatures
for some cause or other. It provides a view of all signatures
(paginated) and a view of the latest 10 signatures (not configurable yet).

## Collecting signatures

The extension does not provide input screens for signatures in the
frontend. Instead, it expects signatures to be collected using
Core TYPO3 forms. The form fields can be saved to the
`tx_petition_domain_model_signature` table by using the
"SaveToDatabase" finisher. A sample configuration is provided
in file `Configuration/Form/SaveToDatabase.yml`. Copy it to
your form definition and adapt according to your field names.

## Credits

Icon for extension, plugin and table derived from
Bullhorn by Vectors Point from the Noun Project.
