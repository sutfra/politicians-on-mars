#
# Table structure for table 'tx_petition_domain_model_signature'
#
CREATE TABLE tx_petition_domain_model_signature (

	name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	location varchar(255) DEFAULT '' NOT NULL,
	message text

);
