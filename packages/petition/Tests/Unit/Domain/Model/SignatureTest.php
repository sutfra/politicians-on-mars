<?php
namespace PoliticiansOnMars\Petition\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Francois Suter <contact@politiciens-sur-mars.ch>
 */
class SignatureTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PoliticiansOnMars\Petition\Domain\Model\Signature
     */
    protected $subject = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = new \PoliticiansOnMars\Petition\Domain\Model\Signature();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertSame(
            'Conceived at T3CON10',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail(): void
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertSame(
            'Conceived at T3CON10',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function getLocationReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getLocation()
        );
    }

    /**
     * @test
     */
    public function setLocationForStringSetsLocation(): void
    {
        $this->subject->setLocation('Conceived at T3CON10');

        self::assertSame(
            'Conceived at T3CON10',
            $this->subject->getLocation()
        );
    }

    /**
     * @test
     */
    public function getMessageReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMessage()
        );
    }

    /**
     * @test
     */
    public function setMessageForStringSetsMessage(): void
    {
        $this->subject->setMessage('Conceived at T3CON10');

        self::assertSame(
            'Conceived at T3CON10',
            $this->subject->getMessage()
        );
    }
}
