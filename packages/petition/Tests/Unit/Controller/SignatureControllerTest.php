<?php
namespace PoliticiansOnMars\Petition\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Francois Suter <contact@politiciens-sur-mars.ch>
 */
class SignatureControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \PoliticiansOnMars\Petition\Controller\SignatureController
     */
    protected $subject = null;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\PoliticiansOnMars\Petition\Controller\SignatureController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function listActionFetchesAllSignaturesFromRepositoryAndAssignsThemToView(): void
    {

        $allSignatures = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $signatureRepository = $this->getMockBuilder(\PoliticiansOnMars\Petition\Domain\Repository\SignatureRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $signatureRepository->expects(self::once())->method('findAll')->will(self::returnValue($allSignatures));
        $this->inject($this->subject, 'signatureRepository', $signatureRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('signatures', $allSignatures);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
