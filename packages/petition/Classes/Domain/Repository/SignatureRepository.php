<?php
declare(strict_types=1);
namespace PoliticiansOnMars\Petition\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;

/***
 *
 * This file is part of the "Petition" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Francois Suter <contact@politiciens-sur-mars.ch>
 *
 ***/

/**
 * Signature repository
 */
class SignatureRepository extends Repository
{

    /**
     * Finds the latest 10 (by default) signatures.
     *
     * @param int $limit Maximum number of signatures to return
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findLatest(int $limit = 10)
    {
        $query = $this->createQuery();
        $query->setOrderings([
                'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
        ])->setLimit($limit);
        return $query->execute();
    }
}