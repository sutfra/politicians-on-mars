<?php
declare(strict_types=1);
namespace PoliticiansOnMars\Petition\Controller;


use PoliticiansOnMars\Petition\Domain\Repository\SignatureRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***
 *
 * This file is part of the "Petition" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Francois Suter <contact@politiciens-sur-mars.ch>
 *
 ***/
/**
 * SignatureController
 */
class SignatureController extends ActionController
{
    protected SignatureRepository $signatureRepository;

    public function injectSignatureRepository(SignatureRepository $repository): void
    {
        $this->signatureRepository = $repository;
    }
    /**
     * action list
     *
     * @return void
     */
    public function listAction(): void
    {
        $signatures = $this->signatureRepository->findAll();
        $this->view->assignMultiple(
                [
                        'signatures' => $signatures
                ]
        );
    }

    /**
     * action latest
     *
     * @return void
     */
    public function latestAction(): void
    {
        $this->view->assign(
                'signatures',
                $this->signatureRepository->findLatest()
        );
    }
}
