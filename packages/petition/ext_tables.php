<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
        function () {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
                    'petition',
                    'SignatureList',
                    'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.list.name',
                    'EXT:petition/Resources/Public/Icons/Extension.svg'
            );
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
                    'petition',
                    'SignatureLatest',
                    'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_signature.latest.name',
                    'EXT:petition/Resources/Public/Icons/Extension.svg'
            );

            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
                    'petition',
                    'Configuration/TypoScript',
                    'Petition'
            );

        }
);
