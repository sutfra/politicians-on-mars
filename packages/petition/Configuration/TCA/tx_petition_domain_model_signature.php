<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_domain_model_signature',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden'
        ],
        'searchFields' => 'name,email,location,message',
        'typeicon_classes' => [
                'default' => 'petition-table-signature'
        ]
    ],
    'interface' => [
        'showRecordFieldList' => 'hidden, name, email, location, message',
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, name, email, location, message'],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],

        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_domain_model_signature.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'email' => [
            'exclude' => false,
            'label' => 'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_domain_model_signature.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'location' => [
            'exclude' => false,
            'label' => 'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_domain_model_signature.location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'message' => [
            'exclude' => false,
            'label' => 'LLL:EXT:petition/Resources/Private/Language/locallang_db.xlf:tx_petition_domain_model_signature.message',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],

    ],
];
